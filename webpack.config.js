const path = require('path');

module.exports = {
  mode: 'development',
  entry: './app/scripts/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  }
};
