export default function () {
  // products slider initiation
  $('.products-slider').slick({
    arrows: true,
    dots: false,
    infinite: true,
    autoplay: true,
    draggable: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          draggable: true
        }
      },
      {
        breakpoint: 575,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          draggable: true
        }
      }
    ]
  });
}
