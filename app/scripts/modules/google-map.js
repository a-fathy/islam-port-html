export default function () {
  // google map initiation
  $(document).ready(function () {
    if ($('#map').length) {
      // get latitude and longitude
      var latitude = Number($('.trinite-map').attr('latitude'));
      var longitude = Number($('.trinite-map').attr('longitude'));
      // Create a map object and specify the DOM element for display.
      var map = new google.maps.Map(document.getElementById('map'), {
        center: {
          lat: latitude,
          lng: longitude
        },
        disableDefaultUI: true,
        zoom: 16,
        scrollwheel: false
      });
      // set map marker
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        map: map
      });
    };
  });
};
