export default function () {
  // main slider initiation
  $('.main-slider').slick({
    dots: true,
    infinite: true,
    autoplay: true,
    draggable: false,
    arrows: false,
    responsive: [{
      breakpoint: 575,
      settings: {
        draggable: true,
      }
    }]
  });
}
