import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import 'popper.js';

import 'bootstrap';

// import 'slick-carousel';

import loader from './modules/loader';
loader();

// import main_slider from './modules/main-slider';
// main_slider();

// import solution_slider from './modules/products-slider';
// solution_slider();

// import google_map from './modules/google-map';
// google_map();
